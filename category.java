import java.util.ArrayList;

public class category
{
    String name;
    ArrayList<product> products;

    category(String name)
    {
        this.name = name;
        products = new ArrayList<>();
    }

    category(String name, ArrayList<product> products)
    {
        this.name = name;
        this.products = products;
    }

    void add_product(product product)
    {
        products.add(product);
    }

    void del_product(int index)
    {
        if (index < products.size())
        {
            products.remove(index);
        }
    }
}
