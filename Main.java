import java.util.ArrayList;

public class Main
{
    public static void main(String[] args)
    {
        user poor_student = new user("Garrow","mail.com","qwerty67890");

        ArrayList<product> eat_category_array = new ArrayList<>();
        eat_category_array.add(new product("Яблоко", 10, 8));
        eat_category_array.add(new product("Чипсы", 70, 9));
        eat_category_array.add(new product("Шоколад", 110, 10));

        category eat = new category("Еда", eat_category_array);

        category kitchen_accessories = new category("Кухонные пренадлежности");
        kitchen_accessories.add_product(new product("Пакет",5,5));
        kitchen_accessories.add_product(new product("Нож",250,10));
        kitchen_accessories.add_product(new product("Тарелка",100,6));

        poor_student.basket.add_product(eat.products.get(0));
        poor_student.basket.add_product(kitchen_accessories.products.get(1));
    }
}
