import java.util.ArrayList;

public class basket
{
    ArrayList<product> products;
    int products_count;

    basket()
    {
        products = new ArrayList<>();
        products_count = 0;
    }

    void add_product(product product)
    {
        products.add(product);
    }

    void del_product(int index)
    {
        if (index < products.size())
        {
            products.remove(index);
        }
    }
}
