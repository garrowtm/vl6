public class user
{
    public String username;
    private long userid;
    private String login;
    private String password;
    basket basket;

    user(String username, String login, String password)
    {
        this.username = username;
        this.login = login;
        this.password = password;
        this.userid = Math.round(Math.abs(Math.random()) * 1000000);
        this.basket = new basket();
    }
}
